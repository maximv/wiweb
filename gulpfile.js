'use strict';

const {src, dest, series, parallel, watch} = require('gulp');
const less = require('gulp-less');
const scss = require('gulp-sass');
const gulpif = require('gulp-if');
const autoprefixer = require('gulp-autoprefixer');
const uglifycss = require('gulp-uglifycss');
const spritesmith = require('gulp.spritesmith');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const buffer = require('vinyl-buffer');
const concat = require('gulp-concat');
const cheerio = require('gulp-cheerio');
const svgSprite = require('gulp-svg-sprite');
const babel = require('gulp-babel');
const minify = require('gulp-minify');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const spriteVersion = 'v73';
const spriteCatVersion = 'v5';

function buildSprite() {
  const spriteData = src('./images/sprite/icons/*.png')
    .pipe(spritesmith({
      imgName: `sprite-${spriteVersion}.png`,
      imgPath: `../../images/sprite-${spriteVersion}.png`,
      cssName: 'sprite.less',
      // cssTemplate: 'images/sprite/icons.css.handlebars',
      retinaSrcFilter: ['images/sprite/icons/*@2x.png'],
      retinaImgName: `sprite-${spriteVersion}@2x.png`,
      retinaImgPath: `../../images/sprite-${spriteVersion}@2x.png`,
      cssOpts: {
        cssSelector: function (sprite) {
          return '.' + sprite.name;
        }
      }
    }));

  const imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(dest('./images/'));

  const cssStream = spriteData.css
    .pipe(dest('./less/sprites/'));

  return merge(imgStream, cssStream);
}

function buildSpriteCategories() {
  const spriteData = src('./images/sprite/category-icons/*.png')
    .pipe(spritesmith({
      imgName: `sprite-categories-${spriteCatVersion}.png`,
      imgPath: `../../images/sprite-categories-${spriteCatVersion}.png`,
      cssName: 'sprite-categories.css',
      // cssTemplate: 'images/sprite/category-icons.css.handlebars',
      retinaSrcFilter: ['images/sprite/category-icons/*@2x.png'],
      retinaImgName: `sprite-categories-${spriteCatVersion}@2x.png`,
      retinaImgPath: `../../images/sprite-categories-${spriteCatVersion}@2x.png`,
      cssOpts: {
        cssSelector: function (sprite) {
          return '.' + sprite.name;
        }
      },
    }));

  const imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(dest('./images/'));

  const cssStream = spriteData.css
    .pipe(dest('./less/sprites'));

  return merge(imgStream, cssStream);
}

function buildSpriteSVG() {
  return src('./images/svg/**/*.svg')
    .pipe(
      gulpif('!**/color/**',
        cheerio({
          run: function ($) {
            $('[fill]').removeAttr('fill');
          },
          parserOptions: {
            xmlMode: true
          }
        }))
    )
    .pipe(svgSprite({
        shape: {
          id: { // SVG shape ID related options
            generator: function (name) {
              return name.replace(/\w+[\/\\]/, '').replace(/.svg$/, '');
            },
          },
        },
        mode: {

          stack: {
            sprite: "../sprite.svg"  //sprite file name
          }
        },
      }
    ))
    .pipe(dest('./images/sprite/svg/'));


}

function buildCss() {
  const lessStream = src('./less/base.less')
    .pipe(less())
    .pipe(autoprefixer({
      cascade: false
    }));

  const scssStream = src('./scss/grid.scss')
    .pipe(scss());

  return merge(scssStream, lessStream)
    .pipe(concat('base.css'))
    //.pipe(gulpif(!isDev, uglifycss()))
    //.pipe(uglifycss())
    .pipe(dest('./dist/css'));
}

function buildTree() {
  const jsTreeStream = src('./less/treeestandalone.less').pipe(less())
    .pipe(autoprefixer({
      cascade: false
    }));

  return jsTreeStream.pipe(concat('jstree.css'))
    //.pipe(gulpif(!isDev, uglifycss()))
    .pipe(uglifycss())
    .pipe(dest('./dist/css'));
}

function buildVendors() {
  return src([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap/dist/js/bootstrap.js',
    './node_modules/selectize.js/dist/js/standalone/selectize.min.js',
    './node_modules/owl.carousel/dist/owl.carousel.min.js',
    './node_modules/jQuery-menu-aim/jquery.menu-aim.js',
    './node_modules/autosize/dist/autosize.js',
    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
    './dist/js/es6/*.js',
  ]).pipe(dest('./dist/js/vendors/'));
}


function buildScripts() {
  return src('./src/es6/*.js')
    .pipe(
      babel({
        presets: ['@babel/preset-env'],
        plugins: ["@babel/plugin-proposal-class-properties"]
      })
    )
    .pipe(
      minify({
        noSource: true,
        ext: {min: '.js'},
      })
    )
    //.pipe(concat('scripts.min.js'))
    .pipe(dest('./dist/js/vendors/'));
}

function watcher() {
  watch('./less/**/*.less', {usePolling: true}, buildCss);
  //watch('./less/**/*.less', {usePolling: true}, series(buildCss, buildTree));
}

exports.sprite = buildSprite;

exports.spriteCategories = buildSpriteCategories;

exports.vendors = buildVendors;

exports.scripts = buildScripts;

exports.watch = series(buildCss, watcher);

exports.build = buildCss;

exports.tree = buildTree;

exports.spriteSVG = buildSpriteSVG;
