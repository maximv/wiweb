<?php

	require_once __DIR__ . '/vendor/autoload.php';

    $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');

    $twig = new Twig_Environment(
        $loader, array(
            'charset' => 'utf-8',
            //'cache' => DIR . '/cache/twig'
        )
    );

    $tpl = (!empty($_GET['path']) && $_GET['path'] != '/') ? $_GET['path'] : 'main';

    require_once __DIR__ . (file_exists(__DIR__ . '/config.php') ? '/config.php' : '/config.php.dist');

	echo $twig->render(
        './' . $tpl . '.twig', array(
            'request_uri' => $_SERVER['REQUEST_URI'],
            'base_path' => $base_path
        )
    );