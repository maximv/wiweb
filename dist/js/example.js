(function ($) {
    $(function () {

        //
        // Fancybox init

        $('[data-fancybox-modal]').fancybox({
            touch: false,
            hash: false,
            arrows: false,
            infobar: false,
        });

        $('.tiny-slider-image').fancybox({
            infobar: false,
            touch: true,
            idleTime: .3,
            afterClose: function() {
                $('.tiny-slider-image').css('display','inline-block');
            }
        });


        //
        // Bootstrap Tooltip & Popover Init

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();


        //
        // Bootstrap Collapse

        $('.btn-hamburger').on('click', function () {
            $(this).toggleClass('collapsed');
            $('#categories').collapse('toggle');
            $('#categories_current').collapse('toggle');

            var breadcrumbs = $(this).closest('.breadcrumbs');

            if (!breadcrumbs.hasClass('divider')) {
                breadcrumbs.data('touch', true);
            }

            if (breadcrumbs.data('touch')) {
                breadcrumbs.toggleClass('divider');
            }
        });


        //
        // Owl carousel v2.3.4

        $('#mastheadBanners').owlCarousel({
            items: 1,
            margin: 0,
            dots: false,
            nav: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: true
        });

        $('#promoBlocks').owlCarousel({
            items: 2,
            margin: 16,
            loop: true,
            nav: true,
            dots: false,
            responsive: {
                992: {
                    items: 4,
                    margin: 20
                },
                768: {
                    items: 3,
                    margin: 20
                },
            }
        });


        //
        // Global Dropdowns

        $('.global-dropdown-close').on('click', function () {
            var dropdown = $(this).closest('div.global-dropdown');

            dropdown.collapse('hide');
        });


        //
        //

        var products = $('[data-products="preview"]');
        var product_item = products.find('.product-item > .product-item-content');

        $('.products-preview-type').on('click', 'span[data-preview-type]', function () {
            var self = $(this);
            var self_data = self.data('preview-type');

            $('span[data-preview-type]').each(function () {
                $(this).removeClass('active');
            });

            self.addClass('active');
            products.attr('class', self_data);

            setHeights();
        });

        setHeights();
        $(window).on('resize', setHeights);


        function setHeights() {
            product_item.css('height', 'auto');

            var per_row = Math.floor(products.width() / product_item.width());

            if (per_row == null || per_row < 2) return true;

            for (var i = 0, j = product_item.length; i < j; i += per_row) {
                var max_height = 0,
                    row = product_item.slice(i, i + per_row);

                row.each(function () {
                    var itemHeight = parseInt($(this).outerHeight());
                    if (itemHeight > max_height) max_height = itemHeight;
                });

                row.css('height', max_height);
            }
        };


        //
        // Product page photos preview
        // Owl carousel v2.3.4

        var product_thumbnails_lg = $('#product_thumbnails_lg');
        var product_thumbnails_sm = $('#product_thumbnails_sm');

        product_thumbnails_lg.owlCarousel({
            items: 1,
            margin: 0,
            dots: false
        });

        product_thumbnails_lg.on('changed.owl.carousel', function (event) {
            var elem_lg_index = event.item.index;
            var elem_sm = product_thumbnails_sm.find('.owl-item')[elem_lg_index];

            $('.thumbnail').each(function () {
                $(this).removeClass('active');
            });

            product_thumbnails_sm.trigger('to.owl.carousel', [elem_lg_index, 200, true]);
            $(elem_sm).find('.thumbnail').addClass('active');
        });

        product_thumbnails_sm.owlCarousel({
            items: 6,
            margin: 0,
            dots: false
        });

        product_thumbnails_sm.on('click', '.owl-item', function () {
            var self = $(this);
            var self_index = self.index();
            var thumbnail = self.find('.thumbnail');

            $('.thumbnail').each(function () {
                $(this).removeClass('active');
            });

            thumbnail.addClass('active');

            product_thumbnails_lg.trigger('to.owl.carousel', [self_index, 200, true]);
        });


        //
        // Menu aim plugin for categories
        // https://github.com/kamens/jQuery-menu-aim

        // $('.nav-categories-list').menuAim({
        //     triggerEvent: 'hover',
        //     rowSelector: '> li',
        //     activate: function(row) {
        //         $(row).find('.nav-subcategory').css('display', 'block');
        //         $(row).closest('.nav-categories-list').addClass('active');
        //         $(row).addClass('hovered');
        //     },
        //     deactivate: function(row) {
        //         $(row).find('.nav-subcategory').css('display', '');
        //         $(row).closest('.nav-categories-list').removeClass('active');
        //         $(row).removeClass('hovered');
        //     },
        //     exitMenu: function(row) {
        //         $(row).find('.nav-subcategory').css('display', '');
        //         $(row).closest('.nav-categories-list').removeClass('active');
        //         $(row).removeClass('hovered');
        //     }
        // });


        $('#categories-aim').find('ul.nav-categories-list').menuAim({
            activate: function(row) {
                var selectedLiNode = $(row).filter('li');

                selectedLiNode.addClass('hovered');
                $(row).closest('div.masthead').addClass('active');

                var childNodes = $(row).find('div.nav-subcategory');
                childNodes.show();

                var floatedBlock = childNodes.find('div.col-xs-12');
                if (floatedBlock.length) {
                    var li_node_top_position = selectedLiNode.position().top;
                    var new_top_position = li_node_top_position - floatedBlock.height() / 2;

                    var new_bottom_position = new_top_position + floatedBlock.height();
                    var max_bottom_position = $(window).scrollTop() + $(window).height() - 300;

                    if (new_bottom_position > max_bottom_position) {
                        new_top_position -= new_bottom_position - max_bottom_position;
                    }

                    if (new_top_position < 0) {
                        new_top_position = 0;
                    } else if (new_bottom_position > childNodes.height()) {
                        new_top_position = childNodes.height() - floatedBlock.height();
                    }

                    floatedBlock.css('top', new_top_position);
                }
            },
            deactivate: function(row) {
                $(row).filter('li').removeClass('hovered');
                $(row).find('div.nav-subcategory').hide();
                $(row).closest('div.masthead').removeClass('active');
            },
            exitMenu: function(list) {
                $(list).find('li.hovered').removeClass('hovered');
                $(list).find('div.nav-subcategory').hide();

                return true;
            }
        });
    });
})(jQuery);
