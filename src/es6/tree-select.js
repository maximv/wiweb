window.addEventListener('load', function () {
    let allTreeSelects = [...document.querySelectorAll('.tree-select')];
    allTreeSelects.forEach((node, i) => new TreeSelect(node, i));
});

class TreeSelect {
    constructor(node,i) {
        this.control = {
            id: i,
            node: node,
            switchCheckbox: node.querySelector('.switch input[type="checkbox"]'),
            chipsList: node.querySelector('.chips-list'),
            chips: [...node.querySelectorAll('.chips-list li')],
            checkboxesList: node.querySelector('.checkboxes-list'),
            checkboxes: [...node.querySelectorAll('.checkboxes-list .custom-checkbox')],
            filter: node.querySelector('.tree-select-filter'),
            collapseButton: node.querySelector('.tree-select-collapse'),
        }
        this.state = {
            excludeSelected: this.control.switchCheckbox.checked,
            collapsed: false
        }
        if (this.state.excludeSelected) {
            this.control.chipsList.classList.add('exclude-selected')
        }
        this.addListeners();
    }

    addListeners() {
        // Exclude-include selected (o )
        this.control.switchCheckbox.addEventListener('change', () => {
            this.control.chipsList.classList.toggle('exclude-selected');
            this.state.excludeSelected = !this.state.excludeSelected;
            this.toggleAllCheckboxes();
        })

        // Click on checkbox [v]
        this.control.checkboxes.forEach(
            (checkbox, i) => {
                let currentCheckbox = checkbox.querySelector('input[type="checkbox"]');
                currentCheckbox.addEventListener('change', (e) => {
                    this.toggleChips(i)
                });
            }
        )

        // Click on chips x
        this.control.chips.forEach(
            (chips, i) => {
                let currentChipsX = chips.querySelector('i');
                currentChipsX.addEventListener('click', (e) => {
                    this.toggleChips(i);
                    this.toggleCheckbox(i);
                });
            }
        )

        // Filter change ab__
        this.control.filter.addEventListener('input', () => this.filterCheckboxes(this.control.filter.value))

        // Collapse click ^
        this.control.collapseButton.addEventListener('click', (e) => {
            this.toggleCollapse();
        });

    }

    toggleAllCheckboxes() {
        this.control.checkboxes.forEach(
            (checkbox, i ) => this.toggleCheckbox(i)
        )
    }

    toggleCheckbox(i) {
        let currentCheckbox = this.control.checkboxes[i].querySelector('input[type="checkbox"]');
        currentCheckbox.checked = !currentCheckbox.checked;
    }

    toggleChips(i) {
        this.control.chips[i].classList.toggle('d-none');
        this.onChangeChipsQuantity();
    }

    toggleCollapse() {
        this.control.collapseButton.classList.toggle('collapsed');
        this.control.node.classList.toggle('collapsed');
        this.state.collapsed = !this.state.collapsed;
    }

    filterCheckboxes(filter) {
        let matchesToStart = (name, filter) => {
            return name.toLowerCase().indexOf(filter.toLowerCase()) == 0;
        }

        this.control.checkboxes.forEach(
            (checkbox, i ) => {
                let name = checkbox.querySelector('label')
                    .innerText
                    .match(/^\s*(.+?)\s*$/)[1];

                if (matchesToStart (name, filter)) {
                    checkbox.classList.remove('filtered')
                } else {
                    checkbox.classList.add('filtered')
                }
            }
        )
    }

    onChangeChipsQuantity() {
        let chipsQuantity = this.countChipsQuantity();
        if (chipsQuantity == 0) {
            if (this.state.collapsed) {
                this.toggleCollapse();
            }
            this.toggleCollapseButton( 'Off');
        } else {
            this.toggleCollapseButton(  'On');
        }
    }

    toggleCollapseButton(position) {
        if (position == 'On') {
            this.control.collapseButton.classList.remove('d-none')
        } else {
            this.control.collapseButton.classList.add('d-none')
        }
    }

    countChipsQuantity() {
        return this.control.chips.reduce(
            function(quantity, item) {
                return (item.classList.contains('d-none')) ? quantity : quantity + 1;
            }, 0);
    }
}