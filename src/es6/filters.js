window.addEventListener('load', function () {
    console.clear();
    const mobileMap = new MobileMap();
    //mobileMap.toggleMapPopup();
});

class TinySlider {
    static IMAGE_SIZE = 64; //px
    static SPACER = 4; //px
    controls;
    props;

    constructor(node) {
        this.controls = {
            node: node.querySelector('.tiny-slider') || console.error('.tiny-slider not found'),
            wrapper: node.querySelector('.tiny-slider-wrapper') || console.error('.tiny-slider-wrapper not found'),
            scrollableLayer: node.querySelector('.tiny-slider-scrollable-layer') || console.error('.tiny-slider-wrapper not found'),
            images: [...node.querySelectorAll('.tiny-slider-image')] || console.error('.tiny-slider-image not found'),
            arrowLeft: node.querySelector('.tiny-slider-left') || console.error('.tiny-slider-left not found'),
            arrowRight: node.querySelector('.tiny-slider-right') || console.error('.tiny-slider-right not found'),
        }
        this.refreshAllProps();
        this.redraw();
        this.addListeners();
    }
    refreshAllProps() {
        this.props = this.props || {};
        this.props.sliderLength = this.controls.node.clientWidth;
        this.props.imagesFit = Math.floor(
            (this.props.sliderLength + TinySlider.SPACER) /
                (TinySlider.IMAGE_SIZE + TinySlider.SPACER)
            );
        this.props.imagesTotal = this.controls.images.length;
        this.props.imagesInView = Math.min(this.props.imagesTotal, this.props.imagesFit);
        this.props.scrolable = this.props.imagesTotal > this.props.imagesInView;
        this.props.viewLength =
            TinySlider.IMAGE_SIZE * this.props.imagesInView +
            TinySlider.SPACER * (this.props.imagesInView - 1);
        this.props.totalLength =
            TinySlider.IMAGE_SIZE * this.props.imagesTotal +
            TinySlider.SPACER * (this.props.imagesTotal - 1);
    }
    redraw() {
        this.controls.wrapper.style.width = this.props.viewLength + "px";
        (this.props.scrolable) ?
            this.controls.node.classList.add('scrollable') :
            this.controls.node.classList.remove('scrollable');
        this.controls.arrowRight.style.left = this.props.viewLength - 12 + "px"
    }
    addListeners() {
        window.addEventListener('refreshed', (e) => {
            this.refreshAllProps();
            this.redraw();
        });
        this.controls.arrowLeft.addEventListener('click', () => this.shiftSlider('left'));
        this.controls.arrowRight.addEventListener('click', () => this.shiftSlider('right'));
    }
    shiftSlider(direction) {
        let currentScrollValue = this.controls.wrapper.scrollLeft;

        let animateSliderShift = (slider, shift) => {
            let startTime = performance.now();
            requestAnimationFrame(function animate(currentTime) {
                let timeFraction = (currentTime - startTime) / 100;
                if (timeFraction > 1) timeFraction = 1;
                slider.scrollLeft = currentScrollValue + timeFraction * shift;

                if (timeFraction < 1) requestAnimationFrame(animate);
            })
        }
        let stepToInt = currentScrollValue % (TinySlider.IMAGE_SIZE + TinySlider.SPACER);
        if (direction === 'left') {
            if (stepToInt === 0) {
                stepToInt = TinySlider.IMAGE_SIZE + TinySlider.SPACER;
            }
            if (stepToInt > 0 && currentScrollValue >= stepToInt) {
                animateSliderShift(this.controls.wrapper, - stepToInt);
            }
        } else {
            stepToInt = (TinySlider.IMAGE_SIZE + TinySlider.SPACER) - stepToInt;
            if (stepToInt === 0) {
                stepToInt = TinySlider.IMAGE_SIZE + TinySlider.SPACER;
            }
            if (stepToInt > 0 && this.props.totalLength - currentScrollValue - this.props.viewLength >= stepToInt) {
                animateSliderShift(this.controls.wrapper, stepToInt);
            }
        }
    }
}

class MobileMap {
    constructor() {
        this.controls = {
            root: document.querySelector('.map-popup') || console.error('.map-popup not found'),
            mapPopup: {
                isOpened: false,
                isAnimated: false,
                layer: document.querySelector('.map-popup') || console.error('.map-popup not found'),
                balloon: document.querySelector('.map-popup-balloon') || console.error('.map-popup-balloon not found'),
                fog: document.querySelector('.map-popup-fog') || console.error('.map-popup-fog not found'),
                closeButton: document.querySelector('.map-popup-balloon-close') || console.error('.map-popup-balloon-close not found'),
            },
            mapDrawer: {
                isExpanded: true,
                isAnimated: false,
                layer: document.querySelector('.map-drawer') || console.error('.map-drawer not found'),
                offersList: document.querySelector('.offers-list') || console.error('.offers-list not found'),
                offers: [...document.querySelectorAll('.offers-list .offer')],
                drawerButton: document.querySelector('.map-drawer-toggle') || console.error('.map-drawer-toggle not found'),
            },
            categoryDropdown: document.querySelector('.category-dropdown') || console.error('No "..." found '),
            burger: document.querySelector('.map-nav-offer .category-back') || console.error('.category-back not found ')
        }
        this.initSliders();
        this.addListeners();
    }
    initSliders() {
        this.controls.mapDrawer.offers.forEach(offer => new TinySlider(offer));
    }

    addListeners() {
         this.controls.categoryDropdown.addEventListener('click', () => {
             this.toggleMapPopup();
         });
        this.controls.mapPopup.closeButton.addEventListener('click', () => {
            this.toggleMapPopup();
        });
        this.controls.mapPopup.fog.addEventListener('click', () => {
            this.toggleMapPopup();
        });
        this.controls.burger.addEventListener('click', () => {
            this.toggleShowMapDrawer();
        });
        this.controls.mapDrawer.drawerButton.addEventListener('click', () => {
            this.toggleExpandMapDrawer();
        });
        window.addEventListener('resize',  this.handleResize());
        //window.addEventListener('toggled', (e) => {console.log('toggled', e)});
    }

    handleResize() {
        let resizeTimeout;
        return function() {
            if (!resizeTimeout) {
                resizeTimeout = setTimeout(function() {
                    resizeTimeout = null;
                    let refreshedEvent = new Event('refreshed');
                    window.dispatchEvent(refreshedEvent);
                }, 300);
            }
        };
    }

    toggleShowMapDrawer() {
        this.controls.mapDrawer.layer.classList.toggle('fully-collapsed');
    }

    toggleExpandMapDrawer() {
        let _drawer = this.controls.mapDrawer;

        if (_drawer.isExpanded) {
            let firstOffer = _drawer.offers[0];
            let collapsedDrawerHeight = firstOffer ? firstOffer.offsetHeight : 40;
            _drawer.layer.style.maxHeight = collapsedDrawerHeight + 'px';
            _drawer.layer.classList.remove('expanded');
        } else {
            _drawer.layer.style.maxHeight = null;
            _drawer.layer.classList.add('expanded');
        }
        _drawer.isExpanded = !_drawer.isExpanded;
    }

    toggleMapPopup() {
        let _mapPopup = this.controls.mapPopup;
        if (!_mapPopup.isAnimated) {
            if (_mapPopup.isOpened) {
                _mapPopup.isAnimated = true;
                let afterBalloonHide = () => {
                    _mapPopup.balloon.removeEventListener('transitionend', afterBalloonHide);
                    let afterFogHide = () => {
                        _mapPopup.fog.removeEventListener('transitionend', afterFogHide);
                        _mapPopup.layer.classList.add('hidden');
                        _mapPopup.isAnimated = false;
                    }
                    _mapPopup.fog.addEventListener('transitionend', afterFogHide);
                    _mapPopup.fog.classList.add('masked');
                }
                _mapPopup.balloon.addEventListener('transitionend', afterBalloonHide);
                _mapPopup.balloon.classList.add('masked');
            } else {
                _mapPopup.isAnimated = true;
                _mapPopup.layer.classList.remove('hidden');
                let afterFogShow = () => {
                    _mapPopup.fog.removeEventListener('transitionend', afterFogShow);
                    let afterBalloonShow = () => {
                        _mapPopup.isAnimated = false;
                    }
                    _mapPopup.balloon.addEventListener('transitionend', afterBalloonShow);
                    _mapPopup.balloon.classList.remove('masked');
                }
                _mapPopup.fog.addEventListener('transitionend', afterFogShow);
                window.requestAnimationFrame(() => _mapPopup.fog.classList.remove('masked'));
            }
            _mapPopup.isOpened = !_mapPopup.isOpened;
            invokeToggledEvent();
        }

        function invokeToggledEvent() {
            //console.log('push toggle event');
            let toggleEvent = new Event('toggled');
            window.dispatchEvent(toggleEvent);
        }
    }
}